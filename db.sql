CREATE DATABASE practica2;

GO
USE practica2;

-- TABLES
CREATE TABLE GradosAcademicos(
    Id BIGINT IDENTITY PRIMARY KEY,
    Nombre NVARCHAR(255) NOT NULL,
    Activo TINYINT NOT NULL DEFAULT 1,
    CONSTRAINT GA_Nombre UNIQUE(Nombre)
);

CREATE TABLE CarrerasProfesionales(
    Id BIGINT IDENTITY PRIMARY KEY,
    Nombre NVARCHAR(255) NOT NULL,
    Activo TINYINT NOT NULL DEFAULT 1,
    GradoAcademicoId BIGINT,
    CONSTRAINT FK_CarreraProfesionalGradoAcademico
        FOREIGN KEY(GradoAcademicoId)
        REFERENCES GradosAcademicos(Id),
    CONSTRAINT CP_Nombre UNIQUE(Nombre)
);

CREATE TABLE DivisionesAcademicas(
    Id BIGINT IDENTITY PRIMARY KEY,
    Nombre NVARCHAR(45) NOT NULL,
    Activo TINYINT NOT NULL DEFAULT 1,
    CONSTRAINT DA_Nombre UNIQUE(Nombre)
);

CREATE TABLE Profesores(
    Id BIGINT IDENTITY PRIMARY KEY,
    Nombre NVARCHAR(45) NOT NULL,
    Apellido1 NVARCHAR(45) NOT NULL,
    Apellido2 NVARCHAR(45),
    CorreoElectronico NVARCHAR(255) NOT NULL,
    FechaIngreso DATE NOT NULL,
    Fotografia NVARCHAR(255) NOT NULL,
    Activo TINYINT NOT NULL DEFAULT 1,
    DivisionAcademicaId BIGINT,
    CarreraProfesionalId BIGINT,
    CONSTRAINT FK_ProfesorDivisionAcademica
        FOREIGN KEY(DivisionAcademicaId)
        REFERENCES DivisionesAcademicas(Id),
    CONSTRAINT FK_ProfesorCarreraProfesional
        FOREIGN KEY(CarreraProfesionalId)
        REFERENCES CarrerasProfesionales(Id)
);

CREATE UNIQUE INDEX Idx_Profesores_CorreoElectronico
    ON Profesores(CorreoElectronico);

CREATE TABLE CarrerasOfertadas(
    Id BIGINT IDENTITY PRIMARY KEY,
    Nombre NVARCHAR(100) NOT NULL,
    PlanEstudios INT NOT NULL,
    Activo TINYINT NOT NULL DEFAULT 1,
    DivisionAcademicaId BIGINT,
    CONSTRAINT FK_CarreraOfertadaDivisionAcademica
        FOREIGN KEY(DivisionAcademicaId)
        REFERENCES DivisionesAcademicas(Id),
    CONSTRAINT CO_Nombre UNIQUE(Nombre)
);

CREATE TABLE Grupos(
    Id BIGINT IDENTITY PRIMARY KEY,
    Cuatrimestre INT NOT NULL,
    Grupo CHAR NOT NULL,
    Generacion VARCHAR(20),
    Activo TINYINT NOT NULL DEFAULT 1,
    CarreraOfertadaId BIGINT,
    ProfesorTutorId BIGINT,
    CONSTRAINT FK_GrupoCarreraOfertada
        FOREIGN KEY(CarreraOfertadaId)
        REFERENCES CarrerasOfertadas(Id),
    CONSTRAINT FK_GrupoProfesor
        FOREIGN KEY(ProfesorTutorId)
        REFERENCES Profesores(Id)
);

CREATE TABLE Estudiantes(
    Id BIGINT IDENTITY PRIMARY KEY,
    Nombre NVARCHAR(45) NOT NULL,
    Apellido1 NVARCHAR(45) NOT NULL,
    Apellido2 NVARCHAR(45),
    Matricula NVARCHAR(45) NOT NULL,
    FechaNacimiento DATE NOT NULL,
    Fotografia NVARCHAR(255) NOT NULL,
    Activo TINYINT NOT NULL DEFAULT 1,
    GrupoId BIGINT,
    CONSTRAINT FK_EstudianteGrupo
        FOREIGN KEY(GrupoId)
        REFERENCES Grupos(Id)
);

CREATE UNIQUE INDEX Idx_Estudiantes_Matricula
    ON Estudiantes(Matricula);

-- INSERTS
SET IDENTITY_INSERT GradosAcademicos ON;
INSERT INTO GradosAcademicos(Id, Nombre)
VALUES
    (1, 'Licenciatura'),
    (2, 'Especialidad'),
    (3, 'Maestría'),
    (4, 'Doctorado');
SET IDENTITY_INSERT GradosAcademicos OFF;

SET IDENTITY_INSERT DivisionesAcademicas ON;
INSERT INTO DivisionesAcademicas(Id, Nombre)
VALUES
    (1, 'DATIC'),
    (2, 'DATEFI'),
    (3, 'DAMI'),
    (4, 'DACEA');
SET IDENTITY_INSERT DivisionesAcademicas OFF;

SET IDENTITY_INSERT CarrerasProfesionales ON;
INSERT INTO CarrerasProfesionales(Id, Nombre, GradoAcademicoId)
VALUES
    (1, 'Ingeniería en Desarrollo y Gestión de Software', 1),
    (2, 'Ingeniería en Redes', 1),
    (3, 'Ingeniería en Mecatrónica', 3),
    (4, 'Ingeniería en Mantenimiento Industrial', 3);
SET IDENTITY_INSERT CarrerasProfesionales OFF;

SET IDENTITY_INSERT CarrerasOfertadas ON;
INSERT INTO CarrerasOfertadas(Id, Nombre, PlanEstudios, DivisionAcademicaId)
VALUES
    (1, 'Ingeniería en Desarrollo y Gestión de Software', 1, 1),
    (2, 'Ingeniería en Redes', 1, 1),
    (3, 'Ingeniería en Mecatrónica', 1, 3),
    (4, 'Ingeniería en Mantenimiento Industrial', 1, 3);
SET IDENTITY_INSERT CarrerasOfertadas OFF;

SET IDENTITY_INSERT Profesores ON;
INSERT INTO Profesores(Id, Nombre, Apellido1, Apellido2, CorreoElectronico, FechaIngreso, Fotografia, DivisionAcademicaId, CarreraProfesionalId)
VALUES
    (1, 'Marco', 'Castrejón', 'Olivares', 'marcoco@ejemplo.com', '2019-08-06', 'https://images.unsplash.com/photo-1485206412256-701ccc5b93ca?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=594&q=80', 1, 1),
    (2, 'Michelle', 'Solíz', 'Rivera', 'michellesr@ejemplo.com', '2018-08-05', 'https://images.unsplash.com/photo-1485893086445-ed75865251e0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80', 3 ,2),
    (3, 'Gerado', 'Román', 'Ramírez', 'gerardorr@ejemplo.com', '2020-08-03', 'https://images.unsplash.com/photo-1484186304838-0bf1a8cff81c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80', 4, 3),
    (4, 'Ariana', 'Rojas', 'Alejandres', 'arianara@ejemplo.com', '2019-02-06', 'https://images.unsplash.com/photo-1484186139897-d5fc6b908812?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80', 2, 4),
    (5, 'Mariano', 'Soriano', 'Hernández', 'marianosh@ejemplo.com', '2019-09-06', 'https://images.unsplash.com/photo-1506207803951-1ee93d7256ad?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80', 1, 1);
SET IDENTITY_INSERT Profesores OFF;

SET IDENTITY_INSERT Grupos ON;
INSERT INTO Grupos(Id, Cuatrimestre, Grupo, Generacion,CarreraOfertadaId, ProfesorTutorId)
VALUES
    (1, 10,'A','27ava',1,3),
    (2, 8,'A','28ava',2,3),
    (3, 7,'A','25ava',1,3),
    (4, 3,'B','30ava',1,3)
    
SET IDENTITY_INSERT Grupos OFF;

SET IDENTITY_INSERT Estudiantes ON;
INSERT INTO Estudiantes(Id, Nombre, Apellido1, Apellido2, Matricula, FechaNacimiento, Fotografia, Activo, GrupoId)
VALUES
    (1, 'Marco', 'Castrejón', 'Olivares', '1234567', '2019-08-06', 'https://images.unsplash.com/photo-1485206412256-701ccc5b93ca?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=594&q=80', 1, 1),
    (2, 'Michelle', 'Solíz', 'Rivera', '1234568', '2018-08-05', 'https://images.unsplash.com/photo-1485893086445-ed75865251e0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80', 0 ,1),
    (3, 'Gerado', 'Román', 'Ramírez', '1234569', '2020-08-03', 'https://images.unsplash.com/photo-1484186304838-0bf1a8cff81c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80', 1, 1),
    (4, 'Ariana', 'Rojas', 'Alejandres', '1234563', '2019-02-06', 'https://images.unsplash.com/photo-1484186139897-d5fc6b908812?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80', 0, 1),
    (5, 'Mariano', 'Soriano', 'Hernández', '1234560', '2019-09-06', 'https://images.unsplash.com/photo-1506207803951-1ee93d7256ad?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80', 1, 1);
SET IDENTITY_INSERT Estudiantes OFF;
