using Microsoft.AspNetCore.Mvc;
using _9APractica2EquipoGandy.Models;
using _9APractica2EquipoGandy.Service;

namespace _9APractica2EquipoGandy.Controllers;

public class DivisionAcademica : Controller
{
    private readonly ILogger<DivisionAcademica> _logger;
    private readonly IDivisionAcademicaRepository _divisionAcademicaRepository;

    public DivisionAcademica(ILogger<DivisionAcademica> logger, IDivisionAcademicaRepository divisionAcademicaRepository)
    {
        _logger = logger;
        _divisionAcademicaRepository = divisionAcademicaRepository;
    }

    public async Task<IActionResult> Index()
    {
        var modelo = new DivisionAcademicaViewModel();
        modelo.DivisionesAcademicas = await _divisionAcademicaRepository.Listar();
        return View(modelo);
    }

    [HttpGet]
    public async Task<IActionResult> Eliminar (long id)
    {
        await _divisionAcademicaRepository.Eliminar(id);
        return RedirectToAction("Index");
    }
}
