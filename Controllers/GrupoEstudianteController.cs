using Microsoft.AspNetCore.Mvc;
using _9APractica2EquipoGandy.Models;
using _9APractica2EquipoGandy.Service;

namespace _9APractica2EquipoGandy.Controllers;


public class GrupoEstudiante : Controller{

    private readonly ILogger<GrupoEstudiante> _logger;
    private readonly IGrupoEstudianteRepository _grupoEstudianteRepository;


    public GrupoEstudiante(ILogger<GrupoEstudiante> logger, IGrupoEstudianteRepository grupoEstudianteRepository)
    {
        _logger = logger;
        _grupoEstudianteRepository = grupoEstudianteRepository;
    }

     public async Task<IActionResult> Index()
    {
        var modelo = new GrupoEstudianteViewModel();
        modelo.GrupoEstudiantes = await _grupoEstudianteRepository.Listar();
        return View(modelo);
    }

    [HttpGet]
    public async Task<IActionResult> Eliminar (long id)
    {
        await _grupoEstudianteRepository.Eliminar(id);
        return RedirectToAction("Index");
    }

}