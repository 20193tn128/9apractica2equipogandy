using Microsoft.AspNetCore.Mvc;
using _9APractica2EquipoGandy.Models;
using _9APractica2EquipoGandy.Service;

namespace _9APractica2EquipoGandy.Controllers;

public class ProfesorController : Controller
{
    private readonly ILogger<ProfesorController> _logger;
    private readonly IProfesorRepository _profesorRepository;

    public ProfesorController(ILogger<ProfesorController> logger, IProfesorRepository profesorRepository)
    {
        _logger = logger;
        _profesorRepository = profesorRepository;
    }

    public async Task<IActionResult> Index()
    {
        var modelo = new ProfesorViewModel();
        modelo.Profesores = await _profesorRepository.Listar();
        return View(modelo);
    }

    [HttpGet]
    public async Task<IActionResult> Eliminar (long id)
    {
        await _profesorRepository.Eliminar(id);
        return RedirectToAction("Index");
    }
}
