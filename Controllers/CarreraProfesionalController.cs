using Microsoft.AspNetCore.Mvc;
using _9APractica2EquipoGandy.Models;
using _9APractica2EquipoGandy.Service;

namespace _9APractica2EquipoGandy.Controllers;

public class CarreraProfesional : Controller
{
    private readonly ILogger<CarreraProfesional> _logger;
    private readonly ICarreraProfesionalRepository _carreraProfesionalRepository;

    public CarreraProfesional(ILogger<CarreraProfesional> logger, ICarreraProfesionalRepository carreraProfesionalRepository)
    {
        _logger = logger;
        _carreraProfesionalRepository = carreraProfesionalRepository;
    }

    public async Task<IActionResult> Index()
    {
        var modelo = new CarreraProfesionalViewModel();
        modelo.CarrerasProfesionales = await _carreraProfesionalRepository.Listar();
        return View(modelo);
    }

    [HttpGet]
    public async Task<IActionResult> Eliminar (long id)
    {
        await _carreraProfesionalRepository.Eliminar(id);
        return RedirectToAction("Index");
    }
}
