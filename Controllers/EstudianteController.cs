using Microsoft.AspNetCore.Mvc;
using _9APractica2EquipoGandy.Models;
using _9APractica2EquipoGandy.Service;

namespace _9APractica2EquipoGandy.Controllers;

public class EstudianteController : Controller
{
    private readonly ILogger<EstudianteController> _logger;
    private readonly IEstudianteRepository _estudianteRepository;

    public EstudianteController(ILogger<EstudianteController> logger, IEstudianteRepository profesorRepository)
    {
        _logger = logger;
        _estudianteRepository = profesorRepository;
    }

    public async Task<IActionResult> Index()
    {
        var modelo = new EstudianteViewModel();
        modelo.Estudiantes = await _estudianteRepository.Listar();
        return View(modelo);
    }

    [HttpGet]
    public async Task<IActionResult> Eliminar (long id)
    {
        await _estudianteRepository.Eliminar(id);
        return RedirectToAction("Index");
    }
}