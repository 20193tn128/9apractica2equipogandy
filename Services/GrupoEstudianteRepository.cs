using Dapper;
using Microsoft.Data.SqlClient;
using _9APractica2EquipoGandy.Models;

namespace _9APractica2EquipoGandy.Service
{
    public interface IGrupoEstudianteRepository
    {
        Task<IEnumerable<GrupoEstudiante>> Listar();
        Task Eliminar(long id);
    }

    public class GrupoEstudianteRepository : IGrupoEstudianteRepository
    {
        private readonly string _connectionString;

        public GrupoEstudianteRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnections");
        }

        public async Task<IEnumerable<GrupoEstudiante>> Listar()
        {
            using var connection = new SqlConnection(_connectionString);
            return await connection.QueryAsync<GrupoEstudiante>("SELECT g.Id, g.Grupo, g.Cuatrimestre, g.Generacion, g.Activo, co.Id AS CarreraOfertadaId, co.Nombre AS CarreraOfertada , pt.Id AS ProfesorTutorId, pt.Nombre AS ProfesorTutor FROM Grupos AS g INNER JOIN CarrerasOfertadas AS co ON g.CarreraOfertadaId = co.Id INNER JOIN Profesores AS pt ON g.ProfesorTutorId = pt.Id;");
        }

        public async Task Eliminar(long id)
        {
            using var connection = new SqlConnection(_connectionString);
            await connection.ExecuteAsync(@"DELETE FROM Grupos WHERE Id = @Id", new { id });
        }

    }
}
