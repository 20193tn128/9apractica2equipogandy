using Dapper;
using Microsoft.Data.SqlClient;
using _9APractica2EquipoGandy.Models;

namespace _9APractica2EquipoGandy.Service
{
    public interface ICarreraProfesionalRepository
    {
        Task<IEnumerable<CarreraProfesional>> Listar();
        Task Eliminar(long id);
    }

    public class CarreraProfesionalRepository : ICarreraProfesionalRepository
    {
        private readonly string _connectionString;

        public CarreraProfesionalRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnections");
        }

        public async Task<IEnumerable<CarreraProfesional>> Listar()
        {
            using var connection = new SqlConnection(_connectionString);
            return await connection.QueryAsync<CarreraProfesional>("SELECT cp.Id, cp.Nombre, cp.Activo, ga.Id AS GradoAcademicoId, ga.Nombre AS GradoAcademico FROM CarrerasProfesionales AS cp INNER JOIN GradosAcademicos AS ga ON cp.GradoAcademicoId = ga.Id ORDER BY cp.Id DESC");
        }

        public async Task Eliminar(long id)
        {
            using var connection = new SqlConnection(_connectionString);
            await connection.ExecuteAsync(@"DELETE FROM CarrerasProfesionales WHERE Id = @Id", new { id });
        }

    }
}
