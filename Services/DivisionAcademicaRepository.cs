using Dapper;
using Microsoft.Data.SqlClient;
using _9APractica2EquipoGandy.Models;

namespace _9APractica2EquipoGandy.Service
{
    public interface IDivisionAcademicaRepository
    {
        Task<IEnumerable<DivisionAcademica>> Listar();
        Task Eliminar(long id);
    }

    public class DivisionAcademicaRepository : IDivisionAcademicaRepository
    {
        private readonly string _connectionString;

        public DivisionAcademicaRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnections");
        }

        public async Task<IEnumerable<DivisionAcademica>> Listar()
        {
            using var connection = new SqlConnection(_connectionString);
            return await connection.QueryAsync<DivisionAcademica>("SELECT Id, Nombre, Activo FROM DivisionesAcademicas;");
        }

        public async Task Eliminar(long id)
        {
            using var connection = new SqlConnection(_connectionString);
            await connection.ExecuteAsync(@"DELETE FROM DivisionesAcademicas WHERE Id = @Id", new { id });
        }
    }
}
