using Dapper;
using Microsoft.Data.SqlClient;
using _9APractica2EquipoGandy.Models;

namespace _9APractica2EquipoGandy.Service
{
    public interface IEstudianteRepository
    {
        Task<IEnumerable<Estudiante>> Listar();
        Task Eliminar(long id);
    }

    public class EstudianteRepository : IEstudianteRepository
    {
        private readonly string _connectionString;

        public EstudianteRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnections");
        }

        public async Task<IEnumerable<Estudiante>> Listar()
        {
            using var connection = new SqlConnection(_connectionString);
            return await connection.QueryAsync<Estudiante>("SELECT e.Id, e.Nombre, e.Apellido1, e.Apellido2, e.Matricula, e.FechaNacimiento,e.Fotografia, e.Activo, g.Id AS GrupoId, g.Grupo as Grupo FROM Estudiantes AS e INNER JOIN Grupos g ON  e.GrupoId = g.Id ;");
        }

        public async Task Eliminar(long id)
        {
            using var connection = new SqlConnection(_connectionString);
            await connection.ExecuteAsync(@"DELETE FROM Estudiantes WHERE Id = @Id", new { id });
        }

    }
}
