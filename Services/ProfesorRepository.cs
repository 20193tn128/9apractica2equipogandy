using Dapper;
using Microsoft.Data.SqlClient;
using _9APractica2EquipoGandy.Models;

namespace _9APractica2EquipoGandy.Service
{
    public interface IProfesorRepository
    {
        Task<IEnumerable<Profesor>> Listar();
        Task Eliminar(long id);
    }

    public class ProfesorRepository : IProfesorRepository
    {
        private readonly string _connectionString;

        public ProfesorRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnections");
        }

        public async Task<IEnumerable<Profesor>> Listar()
        {
            using var connection = new SqlConnection(_connectionString);
            return await connection.QueryAsync<Profesor>("SELECT p.Id, p.Nombre, p.Apellido1, p.Apellido2, p.CorreoElectronico, p.FechaIngreso, p.Fotografia, p.Activo, da.Id AS DivisionAcademicaId, da.Nombre AS DivisionAcademica, cp.Id AS CarreraProfesionalId, cp.Nombre AS CarreraProfesional FROM Profesores AS p INNER JOIN DivisionesAcademicas AS da ON p.DivisionAcademicaId = da.Id INNER JOIN CarrerasProfesionales AS cp ON p.CarreraProfesionalId = cp.Id;");
        }

        public async Task Eliminar(long id)
        {
            using var connection = new SqlConnection(_connectionString);
            await connection.ExecuteAsync(@"DELETE FROM Profesores WHERE Id = @Id", new { id });
        }

    }
}
