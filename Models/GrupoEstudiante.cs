namespace _9APractica2EquipoGandy{


    public class GrupoEstudiante{

        public int Id {
            get;
            set;
        }

        public int Cuatrimestre {
            get; set;
        }

        public char Grupo {
            get;
            set;
        }

        public string? Generacion {
            get;
            set;
        }


        public bool Activo{
            get;
            set;
        }


        public int CarreraOfertadaId {
            get;
            set;
        }


        public int ProfesorTutorId{
            get;
            set;
        }

         public string? CarreraOfertada {
            get;
            set;
        }


        public string? ProfesorTutor{
            get;
            set;
        }



    }


}