namespace _9APractica2EquipoGandy.Models
{
    public class DivisionAcademica 
    {
        public int Id { get; set; }
        public string? Nombre { get; set; }
        public bool Activo { get; set; }
    }
}
