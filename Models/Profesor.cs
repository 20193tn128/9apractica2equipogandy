namespace _9APractica2EquipoGandy.Models
{
    public class Profesor 
    {
        public int Id { get; set; }
        public string? Nombre { get; set; }
        public string? Apellido1 { get; set; }
        public string? Apellido2 { get; set; }
        public string? CorreoElectronico { get; set; }
        public DateTime? FechaIngreso { get; set; }
        public string? Fotografia { get; set; }
        public bool Activo { get; set; }
        public long DivisionAcademicaId { get; set; }
        
        public string? DivisionAcademica { get; set; }
        public long CarreraProfesionalId { get; set; }
        
        public string? CarreraProfesional { get; set; }
    }
}
