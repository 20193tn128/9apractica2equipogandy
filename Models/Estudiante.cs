namespace _9APractica2EquipoGandy.Models
{
    public class Estudiante
    {
        public int Id { get; set; }
        public string? Nombre { get; set; }
        public string? Apellido1 { get; set; }
        public string? Apellido2 { get; set; }
        public string? Matricula { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string? Fotografia { get; set; }
        public bool Activo { get; set; }
        public long GrupoId { get; set; }
        public string? Grupo { get; set; }
    }
}