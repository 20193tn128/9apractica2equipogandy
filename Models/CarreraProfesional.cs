namespace _9APractica2EquipoGandy.Models
{
    public class CarreraProfesional 
    {
        public int Id { get; set; }
        public string? Nombre { get; set; }
        public bool Activo { get; set; }
        public long GradoAcademicoId { get; set; }
        public string? GradoAcademico { get; set; }
    }
}
