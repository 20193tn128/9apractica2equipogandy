namespace _9APractica2EquipoGandy.Models
{
    public class GrupoEstudianteViewModel : GrupoEstudiante
    {
        public IEnumerable<GrupoEstudiante>? GrupoEstudiantes { get; set; }
    }
}
