namespace _9APractica2EquipoGandy.Models
{
    public class CarreraProfesionalViewModel : CarreraProfesional
    {
        public IEnumerable<CarreraProfesional>? CarrerasProfesionales { get; set; }
    }
}
