namespace _9APractica2EquipoGandy.Models
{
    public class DivisionAcademicaViewModel : DivisionAcademica
    {
        public IEnumerable<DivisionAcademica>? DivisionesAcademicas { get; set; }
    }
}
