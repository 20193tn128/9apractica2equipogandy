namespace _9APractica2EquipoGandy.Models
{
    public class ProfesorViewModel : Profesor
    {
        public IEnumerable<Profesor>? Profesores { get; set; }
    }
}
