using _9APractica2EquipoGandy.Service;
using _9APractica2EquipoGandy.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddTransient<IDivisionAcademicaRepository, DivisionAcademicaRepository>();
builder.Services.AddTransient<ICarreraProfesionalRepository, CarreraProfesionalRepository>();
builder.Services.AddTransient<IGrupoEstudianteRepository, GrupoEstudianteRepository>();
builder.Services.AddTransient<IProfesorRepository, ProfesorRepository>();
builder.Services.AddTransient<IEstudianteRepository, EstudianteRepository>();
builder.Services.AddAutoMapper(typeof(Program));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
